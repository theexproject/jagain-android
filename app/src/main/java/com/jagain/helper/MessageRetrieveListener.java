package com.jagain.helper;

/**
 * Created by abdan on 8/20/2017.
 */

public interface MessageRetrieveListener {
    /**
     * Saat temperature berubah
     * @param currTemperature
     */
    void onTemperatureChange(String currTemperature);

    /**
     * Saat HeartRateMonitorModel berubah
     * @param currHeartRate
     */
    void onHeartRateChange(String currHeartRate);
}
