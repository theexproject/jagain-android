package com.jagain.helper;

/**
 * Created by abdan on 8/20/2017.
 */

public class Constant {
    public static final String PORT = ":1883";
    public static final String PROTOCOL = "tcp://";
    public static final String URL = "ngehubx.online";

    public static final String USERNAME = "admintes";
    public static final char[] PASSWORD = "admin123".toCharArray();

    public static final String TOPIC_TEMPERATURE = "jagain/suhu";
    public static final String TOPIC_HEART_RATE = "jagain/bpm";


    //Maksimal pengecekan setiap 5 * 1 menit = 5 menit;
    public static final int MAX_BPM_MEAN = 5;

}
