package com.jagain.helper;

/**
 * Created by abdan on 8/20/2017.
 */

public interface TabChangedListener {
    void onTabChanged(int no, int count);
    void onAlert(String kondisi, String keterangan);
}
