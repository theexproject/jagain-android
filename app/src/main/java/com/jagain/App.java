package com.jagain;

import android.app.Application;

import com.jagain.database.DBHandler;

import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmSchema;

/**
 * Created by abdan on 8/20/2017.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        DBHandler dbHandler = new DBHandler(this);
    }

    @Override
    public void onTerminate() {
        Realm.getDefaultInstance().close();
        super.onTerminate();
    }
}
