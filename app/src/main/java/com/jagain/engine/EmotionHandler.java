package com.jagain.engine;

import android.content.Context;
import android.util.Log;

import com.jagain.database.DBHandler;
import com.jagain.model.EmotionCondition;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;
import io.realm.RealmResults;

/**
 * Created by abdan on 8/20/2017.
 */

public class EmotionHandler {
    private Context mContext;

    public EmotionHandler(Context mContext) {
        this.mContext = mContext;
    }
    
    public void initData(double baseLevel){
        List<RealmObject> mEmotion = new ArrayList<RealmObject>();
        mEmotion.add(new EmotionCondition(0, baseLevel, 14.8, "Distress"));
        mEmotion.add(new EmotionCondition(1, baseLevel, 10.7, "Anger"));
        mEmotion.add(new EmotionCondition(2, baseLevel, -3, "Interest"));
        mEmotion.add(new EmotionCondition(3, baseLevel, -8.4, "Joy"));
        new DBHandler(mContext).add(mEmotion);
    }


    public RealmResults<EmotionCondition> retrieveEmotion() {
        RealmResults<EmotionCondition> result = (RealmResults<EmotionCondition>) new DBHandler(mContext).getAllData(EmotionCondition.class);
        return result;
    }
    public EmotionCondition cekKondisi(double meanRate){
        EmotionCondition result = null;
        for (EmotionCondition emotionCondition : retrieveEmotion()){
            double difference = meanRate - emotionCondition.getBaseLevel();
            if (difference >= emotionCondition.getDifferences()){
                return emotionCondition;
            }
        }
        return result;
    }
}
