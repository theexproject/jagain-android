package com.jagain.engine;

import android.content.Context;
import android.util.Log;

import com.jagain.helper.Constant;
import com.jagain.helper.MessageRetrieveListener;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import static android.content.ContentValues.TAG;

/**
 * Created by abdan on 8/20/2017.
 */

public class IoTHandler {

    String clientId;
    private Context mContext;
    private MessageRetrieveListener listener;
    boolean cek, cek2, cek3;
    static MqttAndroidClient clientMQTT;

    public IoTHandler(Context mContext, MessageRetrieveListener listener) {
        this.mContext = mContext;
        this.listener = listener;
    }

    public void startmqtt() {
        clientId = MqttClient.generateClientId();
        clientMQTT =
                new MqttAndroidClient(mContext, Constant.PROTOCOL + Constant.URL + Constant.PORT,
                        clientId);
        MqttConnectOptions options = new MqttConnectOptions();
        options.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1);
        try {
            options.setUserName(Constant.USERNAME);
            options.setPassword(Constant.PASSWORD);

            IMqttToken token = clientMQTT.connect(options);
            token.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.d("log", "onSuccess" + asyncActionToken);
                    sub();
                    updateData();
                    //  setPublish();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Log.d("log", "onFailure");
                }

            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
//        //MQTT END

    private void sub() {
//        SUBSCRIBE
        String topic2 = Constant.TOPIC_TEMPERATURE;
        String topic3 = Constant.TOPIC_HEART_RATE;
        int qos = 0;
        try {
            IMqttToken subToken2 = clientMQTT.subscribe(topic2, qos);
            subToken2.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // The message was published
                    cek2 = true;
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    cek3 = false;
                }
            });
            IMqttToken subToken3 = clientMQTT.subscribe(topic3, qos);
            subToken3.setActionCallback(new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // The message was published
                    cek3 = true;
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken,
                                      Throwable exception) {
                    cek3 = false;
                }
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private void updateData() {
        clientMQTT.setCallback(new MqttCallback() {

            @Override
            public void connectionLost(Throwable cause) { //Called when the client lost the connection to the broker
                Log.d("COBA", "connectionLost: " + "LOSS");
            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                //String simpan = Arrays.toString(message.getPayload());
                byte[] pesan = message.getPayload();
                int i = pesan.length;
                String msg = "";
                for (int i1 = 0; i1 < i; i1++) {
                    msg += String.valueOf(Character.toChars(pesan[i1]));
                }
                System.out.println(topic + ": " + msg);
                String simpan = msg;

                // text.setText(msg);
                System.out.println("topicnya = " + topic);
                if (topic.equals(Constant.TOPIC_HEART_RATE)) {
                    Log.d(TAG, "messageArrived: "+simpan);
                    listener.onHeartRateChange(simpan);
                } else if (topic.equals(Constant.TOPIC_TEMPERATURE)) {
                    listener.onTemperatureChange(simpan);
//                    mTemperature.setValue(Integer.parseInt(simpan));
                }

            }


            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {//Called when a outgoing publish is complete
                Log.d("COBA", "connectionLost: " + "LsOSS");
            }
        });
    }
}
