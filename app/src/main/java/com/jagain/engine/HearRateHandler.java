package com.jagain.engine;

import android.content.Context;

import com.jagain.database.DBHandler;
import com.jagain.model.HeartRateCondition;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by abdan on 8/20/2017.
 */

public class HearRateHandler {
    private Context mContext;
    public HearRateHandler(Context mContext) {
        this.mContext = mContext;
    }

    public void initData(){
        List<RealmObject> mHeartRate = new ArrayList<RealmObject>();
        mHeartRate.add(new HeartRateCondition(0, 70, 190, "0-1 bulan"));
        mHeartRate.add(new HeartRateCondition(1, 80, 160, "1-11 bulan"));
        mHeartRate.add(new HeartRateCondition(2, 80, 130, "1-2 tahun"));
        mHeartRate.add(new HeartRateCondition(3, 80, 120, "3-4 tahun"));
        mHeartRate.add(new HeartRateCondition(4, 75, 110, "5-6 tahun"));
        mHeartRate.add(new HeartRateCondition(5, 70, 110, "7-9 tahun"));
        mHeartRate.add(new HeartRateCondition(6, 60, 110, "10 tahun keatas"));
        new DBHandler(mContext).add(mHeartRate);
    }

    public RealmResults<HeartRateCondition> retrieve() {
        RealmResults<HeartRateCondition> result = (RealmResults<HeartRateCondition>) new DBHandler(mContext).getAllData(HeartRateCondition.class);
        return result;
    }

    public boolean isNormal(int kondisi, double meanRate){
        if ((meanRate >= retrieve().get(kondisi).getMinRate()) && (meanRate <= retrieve().get(kondisi).getMaxRate()))
            return false;
        else return true;
    }

}
