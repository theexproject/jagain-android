package com.jagain.adapter;

import android.app.Notification;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.jagain.R;
import com.jagain.model.Notif;

import java.util.List;

import io.realm.RealmResults;

/**
 * Created by abdan on 7/29/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {
    private Context mContext;
    private RealmResults<Notif> mNotif;

    public NotificationAdapter(Context mContext, RealmResults<Notif> mNotif) {
        this.mContext = mContext;
        this.mNotif = mNotif;
    }

    @Override
    public NotificationAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.card_notification, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int position) {
        holder.mJudul.setText(mNotif.get(position).getJudul());
        holder.mWaktu.setText(mNotif.get(position).getWaktu());
        holder.mFoto.setBackgroundResource(Integer.valueOf(mNotif.get(position).getTipe()));
    }

    @Override
    public int getItemCount() {
        return mNotif.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mFoto, mNext;
        public TextView mJudul, mWaktu;

        public ViewHolder(View itemView) {
            super(itemView);

            mFoto = (ImageView) itemView.findViewById(R.id.card_notification_imgFoto);
            mWaktu = (TextView) itemView.findViewById(R.id.card_notification_tvWaktu);
            mJudul = (TextView) itemView.findViewById(R.id.card_notification_tvJudul);
            mNext = (ImageView) itemView.findViewById(R.id.card_notification_imgNext);
        }
    }
}
