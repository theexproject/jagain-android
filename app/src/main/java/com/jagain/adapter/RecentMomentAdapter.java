package com.jagain.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jagain.R;
import com.jagain.model.Moment;
import com.jagain.model.monitor.EmotionMonitorModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.RealmResults;

/**
 * Created by abdan on 8/19/2017.
 */

public class RecentMomentAdapter extends RecyclerView.Adapter<RecentMomentAdapter.ViewHolder> {
    private Context mContext;
    private RealmResults<EmotionMonitorModel> mMoment;
    private ViewGroup parent;

    public RecentMomentAdapter(Context mContext, RealmResults<EmotionMonitorModel> mMoment) {
        this.mContext = mContext;
        this.mMoment = mMoment;
    }

    public void setMomentList(RealmResults<EmotionMonitorModel> mMoment){
        this.mMoment = mMoment;
        notifyDataSetChanged();
    }
    @Override
    public RecentMomentAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.card_moments, parent, false);
        this.parent = parent;
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecentMomentAdapter.ViewHolder holder, final int position) {
        final EmotionMonitorModel moment = mMoment.get(position);
        holder.tvMoment.setText(moment.getEmotion());
        if (moment.getEmotion().equalsIgnoreCase("Interest")){
            holder.imgMoment.setBackgroundResource(R.drawable.ketawa);
        }else if (moment.getEmotion().equalsIgnoreCase("Anger")){
            holder.imgMoment.setBackgroundResource(R.drawable.nangis);
        }else if (moment.getEmotion().equalsIgnoreCase("Distress")){
            holder.imgMoment.setBackgroundResource(R.drawable.vomit);
        }else{
            holder.imgMoment.setBackgroundResource(R.drawable.tidur);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return mMoment.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_menu_imgMoment)
        ImageView imgMoment;

        @BindView(R.id.card_menu_tvMoment)
        TextView tvMoment;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
