package com.jagain.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.jagain.activity.home.fragment.growth.GraphFragment;
import com.jagain.activity.home.fragment.growth.StatisticsFragment;

/**
 * Created by abdan on 8/21/2017.
 */

public class GrowthPagerAdapter extends FragmentPagerAdapter {
    final int PAGE_COUNT = 2;
    private String tabTitles[] = new String[] { "Statistics", "Graph"};
    private Context context;

    public GrowthPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new StatisticsFragment();
            default:
                return new GraphFragment();
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }
}
