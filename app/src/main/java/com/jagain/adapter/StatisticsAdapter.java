package com.jagain.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jagain.R;
import com.jagain.model.monitor.EmotionMonitorModel;
import com.jagain.model.monitor.TemperatureMonitorModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;

/**
 * Created by abdan on 8/19/2017.
 */

public class StatisticsAdapter extends RecyclerView.Adapter<StatisticsAdapter.ViewHolder> {
    private Context mContext;
    private List<TemperatureMonitorModel> mTemperature;

    public StatisticsAdapter(Context mContext, List<TemperatureMonitorModel> mTemperature) {
        this.mContext = mContext;
        this.mTemperature = mTemperature;
    }


    public void setmTemperatureList(RealmResults<TemperatureMonitorModel> mTemperature){
        this.mTemperature = mTemperature;
        notifyDataSetChanged();
    }
    @Override
    public StatisticsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.card_temperature, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(StatisticsAdapter.ViewHolder holder, int position) {
        TemperatureMonitorModel temperatureMonitorModel = mTemperature.get(position);
        holder.mTemp.setText(String.valueOf(temperatureMonitorModel.getTemperature()) + " °C");
        holder.mCondition.setText(temperatureMonitorModel.getKondisi());
    }

    @Override
    public int getItemCount() {
        return mTemperature.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.card_temperature_tvTemp)
        TextView mTemp;
        
        @BindView(R.id.card_temperature_tvCondition)
        TextView mCondition;
        
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
