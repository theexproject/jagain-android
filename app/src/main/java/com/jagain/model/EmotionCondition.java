package com.jagain.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by abdan on 8/20/2017.
 */

public class EmotionCondition extends RealmObject{
    @PrimaryKey
    private int id;
    private double baseLevel, differences;
    private String kondisi;

    public EmotionCondition() {
    }

    public EmotionCondition(int id, double baseLevel, double differences, String kondisi) {
        this.id = id;
        this.baseLevel = baseLevel;
        this.differences = differences;
        this.kondisi = kondisi;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getBaseLevel() {
        return baseLevel;
    }

    public void setBaseLevel(double baseLevel) {
        this.baseLevel = baseLevel;
    }

    public double getDifferences() {
        return differences;
    }

    public void setDifferences(double differences) {
        this.differences = differences;
    }
}
