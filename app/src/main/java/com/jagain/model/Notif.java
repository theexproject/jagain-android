package com.jagain.model;

/**
 * Created by abdan on 7/29/2017.
 */
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Notif extends RealmObject {
    @PrimaryKey
    private int id;
    private String judul;
    private String waktu;
    private String tipe;

    public Notif() {
    }

    public Notif(int id, String judul, String waktu, String tipe) {
        this.id = id;
        this.judul = judul;
        this.waktu = waktu;
        this.tipe = tipe;
    }

    public int getIdnotif() {
        return id;
    }

    public void setIdnotif(int idnotif) {
        this.id = idnotif;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getTipe() {
        return tipe;
    }

    public void setTipe(String tipe) {
        this.tipe = tipe;
    }

}