package com.jagain.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by abdan on 8/20/2017.
 */

public class Moment extends RealmObject {
    @PrimaryKey
    private int id;
    private String waktu, kondisi;

    public Moment() {
    }

    public Moment(int id, String waktu, String kondisi) {
        this.id = id;
        this.waktu = waktu;
        this.kondisi = kondisi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }
}
