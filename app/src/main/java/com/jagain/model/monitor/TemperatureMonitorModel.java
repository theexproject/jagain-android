package com.jagain.model.monitor;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by abdan on 8/20/2017.
 */

public class TemperatureMonitorModel extends RealmObject {
    @PrimaryKey
    private int id;
    private double temperature;
    private String time, kondisi;

    public TemperatureMonitorModel() {
    }

    public TemperatureMonitorModel(int id, double temperature, String time, String kondisi) {
        this.id = id;
        this.temperature = temperature;
        this.time = time;
        this.kondisi = kondisi;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
