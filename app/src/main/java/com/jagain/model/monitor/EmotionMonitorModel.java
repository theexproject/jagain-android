package com.jagain.model.monitor;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by abdan on 8/20/2017.
 */

public class EmotionMonitorModel extends RealmObject {
    @PrimaryKey
    private int id;
    private String emotion;
    private String time;

    public EmotionMonitorModel() {
    }

    public EmotionMonitorModel(int id, String emotion, String time) {
        this.id = id;
        this.emotion = emotion;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmotion() {
        return emotion;
    }

    public void setEmotion(String emotion) {
        this.emotion = emotion;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
