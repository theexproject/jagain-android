package com.jagain.model.monitor;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by abdan on 8/20/2017.
 */

public class HeartRateMonitorModel extends RealmObject {
    @PrimaryKey
    private int id;
    private double heartRate;
    private String time;

    public HeartRateMonitorModel() {
    }

    public HeartRateMonitorModel(int id, double heartRate, String time) {
        this.id = id;
        this.heartRate = heartRate;
        this.time = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(double heartRate) {
        this.heartRate = heartRate;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
