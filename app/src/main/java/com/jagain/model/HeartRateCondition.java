package com.jagain.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by abdan on 8/20/2017.
 */

public class HeartRateCondition extends RealmObject {
    @PrimaryKey
    private int id;
    private double minRate, maxRate;
    private String keterangan;

    public HeartRateCondition() {
    }

    public HeartRateCondition(int id, double minRate, double maxRate, String keterangan) {
        this.id = id;
        this.minRate = minRate;
        this.maxRate = maxRate;
        this.keterangan = keterangan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getMinRate() {
        return minRate;
    }

    public void setMinRate(double minRate) {
        this.minRate = minRate;
    }

    public double getMaxRate() {
        return maxRate;
    }

    public void setMaxRate(double maxRate) {
        this.maxRate = maxRate;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
