package com.jagain.activity.splash;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jagain.activity.home.MainActivity;
import com.jagain.R;
import com.jagain.activity.login.LoginActivity;


public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep(2000);
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    if (sharedPref.getString("TOKEN", "null").equals("null")){
                        Intent intent = new Intent(getBaseContext(), LoginActivity.class);
                        intent.putExtra("role", 0);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent intent = new Intent(getBaseContext(), MainActivity.class);
                        intent.putExtra("role", 0);
                        startActivity(intent);
                        finish();
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }
}
