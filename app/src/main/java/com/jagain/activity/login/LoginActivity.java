package com.jagain.activity.login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jagain.R;
import com.jagain.activity.welcome.WelcomeActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private final int REGISTER_ACTIVITY_RESPONSE = 1;

    @BindView(R.id.login_pgLogin)
    ProgressBar mProgressBarLogin;

    @BindView(R.id.login_etEmail)
    EditText mEmail;

    @BindView(R.id.login_etPassword)
    EditText mPassword;

    @BindView(R.id.login_btnLogin)
    Button mBtnLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mBtnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                mBtnLogin.setVisibility(View.INVISIBLE);
                mProgressBarLogin.setVisibility(View.VISIBLE);

                if (mEmail.getText().toString().equals("jagain@gmail.com")
                        && mPassword.getText().toString().equals("sandibaru")) {
                    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                    SharedPreferences.Editor editor = sharedPref.edit();
                    //TODO
                    editor.putInt("role", 3);
                    editor.apply();

                    Intent intent = new Intent(getBaseContext(), WelcomeActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    mBtnLogin.setVisibility(View.VISIBLE);
                    mProgressBarLogin.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Email dan Password salah", Toast.LENGTH_SHORT).show();
                }
        }
    }
}
