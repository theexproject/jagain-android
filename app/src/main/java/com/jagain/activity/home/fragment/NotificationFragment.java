package com.jagain.activity.home.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.jagain.R;
import com.jagain.adapter.NotificationAdapter;
import com.jagain.database.DBHandler;
import com.jagain.model.Notif;
import com.jagain.model.monitor.EmotionMonitorModel;
import com.jagain.model.monitor.TemperatureMonitorModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by abdan on 8/19/2017.
 */

public class NotificationFragment extends Fragment{
    private int kondisi = 1;
    @BindView(R.id.notification_rvNotif)
    RecyclerView mNotif;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_notification, container, false);
        ButterKnife.bind(this, v);
        NotificationAdapter notificationAdapter = new NotificationAdapter(getContext(), retrieveNotification());
        mNotif.setAdapter(notificationAdapter);
        mNotif.setLayoutManager(new LinearLayoutManager(getContext()));
        return v;
    }

    public RealmResults<Notif> retrieveNotification() {
        RealmResults<Notif> result = (RealmResults<Notif>) new DBHandler(getContext()).getAllData(Notif.class);
        return result;
    }

}
