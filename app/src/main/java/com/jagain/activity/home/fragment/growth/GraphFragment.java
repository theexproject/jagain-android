package com.jagain.activity.home.fragment.growth;

import android.support.v4.app.Fragment;

/**
 * Created by abdan on 8/21/2017.
 */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.jagain.R;
import com.jagain.database.DBHandler;
import com.jagain.model.monitor.HeartRateMonitorModel;
import com.jagain.model.monitor.TemperatureMonitorModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;

/**
 * Created by abdan on 8/21/2017.
 */

public class GraphFragment extends Fragment {
    @BindView(R.id.chartBPM)
    LineChart chart;

    @BindView(R.id.chartTemperature)
    LineChart mJoy;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_graph, container, false);
        ButterKnife.bind(this, v);
        // Prepare data
        List<Entry> entries = new ArrayList<Entry>();
        int count = 0;
        for (TemperatureMonitorModel temperatureMonitorModel : retrieveTemperature()){
            entries.add(new Entry(count++, (float) temperatureMonitorModel.getTemperature()));
        }
        LineDataSet dataSet = new LineDataSet(entries, "Label"); // add entries to dataset

        dataSet.setDrawValues(false);

        ArrayList<LineDataSet> dataSets = new ArrayList<>();
        dataSets.add(dataSet);
        dataSet.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        LineData lineData = new LineData(dataSet);
        mJoy.setData(lineData);

        // style mJoy
        mJoy.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.Azure)); // use your bg color
        mJoy.setDescription(null);
        mJoy.setDrawGridBackground(false);
        mJoy.setDrawBorders(false);

        mJoy.setAutoScaleMinMaxEnabled(true);

        // remove axis
        YAxis leftAxis = mJoy.getAxisLeft();
        leftAxis.setEnabled(false);

        XAxis xAxis = mJoy.getXAxis();
        xAxis.setEnabled(false);

        // hide legend
        Legend legend = mJoy.getLegend();
        legend.setEnabled(false);
        mJoy.invalidate();





        List<Entry> entriesTemp = new ArrayList<Entry>();
        int countTemp = 0;
        for (HeartRateMonitorModel temperatureMonitorModel : retrieveHeartRate()){
            entriesTemp.add(new Entry(countTemp++, (float) temperatureMonitorModel.getHeartRate()));
        }
        LineDataSet dataSetTemp = new LineDataSet(entriesTemp, "Label"); // add entries to dataset

        dataSetTemp.setDrawValues(false);

        ArrayList<LineDataSet> dataSetsTemp = new ArrayList<>();
        dataSetsTemp.add(dataSetTemp);
        dataSetTemp.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        dataSetTemp.setColor(ContextCompat.getColor(getContext(), R.color.neon_pink));
        dataSetTemp.setCircleColor(ContextCompat.getColor(getContext(), R.color.neon_pink));
        LineData lineDataTemp = new LineData(dataSetTemp);
        chart.setData(lineDataTemp);

        // style chart
        chart.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.Azure)); // use your bg color
        chart.setDescription(null);
        chart.setDrawGridBackground(false);
        chart.setDrawBorders(false);

        chart.setAutoScaleMinMaxEnabled(true);

        // remove axis
        YAxis leftAxisTemp = chart.getAxisLeft();
        leftAxisTemp.setEnabled(false);

        XAxis xAxisTemp = chart.getXAxis();
        xAxisTemp.setEnabled(false);

        // hide legend
        Legend legendTemp = chart.getLegend();
        legendTemp.setEnabled(false);
        chart.invalidate();


        return v;
    }


    public RealmResults<TemperatureMonitorModel> retrieveTemperature() {
        RealmResults<TemperatureMonitorModel> result = (RealmResults<TemperatureMonitorModel>) new DBHandler(getContext()).getAllData(TemperatureMonitorModel.class);
        return result;
    }



    public RealmResults<HeartRateMonitorModel> retrieveHeartRate() {
        RealmResults<HeartRateMonitorModel> result = (RealmResults<HeartRateMonitorModel>) new DBHandler(getContext()).getAllData(HeartRateMonitorModel.class);
        return result;
    }
}
