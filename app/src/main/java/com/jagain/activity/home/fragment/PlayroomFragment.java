package com.jagain.activity.home.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jagain.R;
import com.jagain.adapter.RecentMomentAdapter;
import com.jagain.adapter.StatisticsAdapter;
import com.jagain.component.DigitTextView;
import com.jagain.database.DBHandler;
import com.jagain.engine.EmotionHandler;
import com.jagain.engine.HearRateHandler;
import com.jagain.engine.IoTHandler;
import com.jagain.helper.Constant;
import com.jagain.helper.MessageRetrieveListener;
import com.jagain.engine.TemperatureHandler;
import com.jagain.helper.TabChangedListener;
import com.jagain.model.EmotionCondition;
import com.jagain.model.Moment;
import com.jagain.model.Notif;
import com.jagain.model.monitor.EmotionMonitorModel;
import com.jagain.model.monitor.HeartRateMonitorModel;
import com.jagain.model.monitor.TemperatureMonitorModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmResults;

import static android.content.ContentValues.TAG;

/**
 * Created by abdan on 8/19/2017.
 */

public class PlayroomFragment extends Fragment implements MessageRetrieveListener{

    @BindView(R.id.playroom_tvRate)
    DigitTextView mRate;

    @BindView(R.id.playroom_tvTemperature)
    DigitTextView mTemperature;

    @BindView(R.id.beranda_rvMoments)
    RecyclerView rvMoments;

    @BindView(R.id.beranda_rvTemperature)
    RecyclerView rvTemperature;

    @BindView(R.id.beranda_imgProfil)
    ImageView imgProfil;

    private EmotionHandler emotionHandler;
    private TemperatureHandler temperatureHandler;
    private HearRateHandler hearRateHandler;


    private double meanHeartRate;
    private int nHeartRate = 0, nTemperature=0, nEmotion=0;
    TabChangedListener listener;

    List<Moment> mMoment;
    private RecentMomentAdapter adapter;
    private StatisticsAdapter temperatureAdapter;
    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_playroom, container, false);
        ButterKnife.bind(this, v);
        mRate.setColor(ContextCompat.getColor(getContext(), R.color.neon_pink));
        mTemperature.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
        IoTHandler ioTHandler = new IoTHandler(getContext(), this);
        ioTHandler.startmqtt();

        mRate.setNormalValue(99);
        mTemperature.setNormalValue(28);

        mRate.setValue(103);
        mTemperature.setValue(32);
        initHandler();
        try{
            initDummy();
        }catch (Exception e){
            e.printStackTrace();
        }
        adapter = new RecentMomentAdapter(getContext(), retrieveEmotion());
        rvMoments.setAdapter(adapter);
        rvMoments.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        temperatureAdapter = new StatisticsAdapter(getContext(), retrieveTemperature());
        rvTemperature.setAdapter(temperatureAdapter);
        rvTemperature.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        imgProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(v.getContext());
                View mView = inflater.inflate(R.layout.dialog_gambar, container, false);
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                dialog.show();
            }
        });
        return v;
    }

    private void initDummy(){

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("happy", 30);
        editor.putInt("joy", 23);
        editor.putInt("stress", 15);
        editor.putInt("anger", 2);
        editor.commit();


        List<RealmObject> mMoment = new ArrayList<RealmObject>();
        mMoment.add(new EmotionMonitorModel(0, "Interest", "null"));
        mMoment.add(new EmotionMonitorModel(1, "Anger", "null"));
        mMoment.add(new EmotionMonitorModel(2, "Distress", "null"));
        mMoment.add(new EmotionMonitorModel(3, "Joy", "null"));
        new DBHandler(getContext()).add(mMoment);

        List<RealmObject> mTemperature = new ArrayList<RealmObject>();
        mTemperature.add(new TemperatureMonitorModel(0, 32, "null", "Normal"));
        mTemperature.add(new TemperatureMonitorModel(1, 30, "null", "Normal"));
        mTemperature.add(new TemperatureMonitorModel(2, 31, "null", "Normal"));
        mTemperature.add(new TemperatureMonitorModel(3, 29, "null", "Normal"));
        new DBHandler(getContext()).add(mTemperature);

        List<RealmObject> mNotif = new ArrayList<RealmObject>();
        mNotif.add(new Notif(0, "Device telah terkoneksi", "null", String.valueOf(R.drawable.icon_circle_centang)));
        mNotif.add(new Notif(1, "Device tersambung dengan bayi", "null", String.valueOf(R.drawable.icon_circle_user)));
        new DBHandler(getContext()).add(mNotif);
        emotionHandler.initData(103);
        hearRateHandler.initData();

    }
    private void initHandler(){
        //Init EmotionCondition
        //BaseLevel BPM 103
        emotionHandler = new EmotionHandler(getContext());

        //Init HeartRateMonitorModel
        hearRateHandler = new HearRateHandler(getContext());
        temperatureHandler = new TemperatureHandler();

    }

    @Override
    public void onTemperatureChange(String currTemperature) {
        double dTemperature = Double.parseDouble(currTemperature);
        mTemperature.setValue(dTemperature);
        if (!temperatureHandler.isNormal(dTemperature)){
            listener.onTabChanged(2, 1);
            listener.onAlert("Temperature is not normal", "Temperature");
            new DBHandler(getContext()).add(new TemperatureMonitorModel(getNextTemperatureKey()+1, dTemperature, "null", "Not Normal"));
        }else{
            new DBHandler(getContext()).add(new TemperatureMonitorModel(getNextTemperatureKey()+1, dTemperature, "null", "Normal"));
        }
        temperatureAdapter.setmTemperatureList(retrieveTemperature());
    }

    @Override
    public void onHeartRateChange(String currHeartRate) {
        double dHeartRate = Double.parseDouble(currHeartRate);
        mRate.setValue(dHeartRate);
        if (!hearRateHandler.isNormal(6, dHeartRate)){
            listener.onTabChanged(2, 1);
//            listener.onAlert("Heart Rate is not normal", "Heart Rate");
        }
        ++nHeartRate;
        meanHeartRate = (meanHeartRate * nHeartRate) + dHeartRate;
        if (nHeartRate == Constant.MAX_BPM_MEAN){
            EmotionCondition emotionCondition = emotionHandler.cekKondisi(meanHeartRate);
            meanHeartRate = 0;
            nHeartRate = 0;
            if (emotionCondition != null){
                listener.onTabChanged(2, 1);
                listener.onAlert(emotionCondition.getKondisi(), "EmotionCondition");


                SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
                int nHappy = sharedPref.getInt("happy", 0);
                int nJoy = sharedPref.getInt("joy", 0);
                int nStress = sharedPref.getInt("stress", 0);
                int nAnger = sharedPref.getInt("anger", 0);

                if (emotionCondition.getKondisi().equalsIgnoreCase("Interest")){
                    nHappy++;
                }else if (emotionCondition.getKondisi().equalsIgnoreCase("Anger")){
                    nAnger++;
                }else if (emotionCondition.getKondisi().equalsIgnoreCase("Distress")){
                    nStress++;
                }else{
                    nJoy++;
                }
                SharedPreferences.Editor editor = sharedPref.edit();
                //TODO
                editor.putInt("happy", nHappy);
                editor.putInt("joy", nJoy);
                editor.putInt("stress", nStress);
                editor.putInt("anger", nAnger);
                editor.commit();
            }
            new DBHandler(getContext()).add(new EmotionMonitorModel(getNextEmotionKey()+1, emotionCondition.getKondisi(), "null"));
            adapter.setMomentList(retrieveEmotion());
            new DBHandler(getContext()).add(new HeartRateMonitorModel(getNextHeartRateKey()+1, dHeartRate, "null"));
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (TabChangedListener) context;
    }

    public RealmResults<EmotionMonitorModel> retrieveEmotion() {
        RealmResults<EmotionMonitorModel> result = (RealmResults<EmotionMonitorModel>) new DBHandler(getContext()).getAllData(EmotionMonitorModel.class);
        return result;
    }

    public RealmResults<TemperatureMonitorModel> retrieveTemperature() {
        RealmResults<TemperatureMonitorModel> result = (RealmResults<TemperatureMonitorModel>) new DBHandler(getContext()).getAllData(TemperatureMonitorModel.class);
        return result;
    }

    public int getNextTemperatureKey(){
        try{
            Number number = Realm.getDefaultInstance().where(TemperatureMonitorModel.class).max("id");
            if (number!=null){
                return number.intValue();
            }else{
                return 0;
            }
        }catch (ArrayIndexOutOfBoundsException e){
            return 0;
        }
    }

    public int getNextEmotionKey(){
        try{
            Number number = Realm.getDefaultInstance().where(EmotionMonitorModel.class).max("id");
            if (number!=null){
                return number.intValue();
            }else{
                return 0;
            }
        }catch (ArrayIndexOutOfBoundsException e){
            return 0;
        }
    }


    public int getNextHeartRateKey(){
        try{
            Number number = Realm.getDefaultInstance().where(HeartRateMonitorModel.class).max("id");
            if (number!=null){
                return number.intValue();
            }else{
                return 0;
            }
        }catch (ArrayIndexOutOfBoundsException e){
            return 0;
        }
    }
}
