package com.jagain.activity.home;

import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.jagain.R;
import com.jagain.activity.home.fragment.growth.GrowthFragment;
import com.jagain.activity.home.fragment.NotificationFragment;
import com.jagain.activity.home.fragment.PlayroomFragment;
import com.jagain.activity.home.fragment.ProfileFragment;
import com.jagain.database.DBHandler;
import com.jagain.helper.TabChangedListener;
import com.jagain.model.Notif;
import com.jagain.model.monitor.EmotionMonitorModel;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.BottomBarTab;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmObject;

public class MainActivity extends AppCompatActivity implements OnTabSelectListener, TabChangedListener {

    @BindView(R.id.main_bottomBar)
    BottomBar bottomBar;

    @BindView(R.id.beranda_toolbar)
    Toolbar toolbar;

    private int notifCount=0;
    private int numb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        bottomBar.setOnTabSelectListener(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle !=null){
            numb = bundle.getInt("kebiasaan");
            if (numb == 0){
                BottomBarTab nearby = bottomBar.getTabWithId(R.id.tab_lahan);
                nearby.setVisibility(View.GONE);
            }else{
                BottomBarTab nearby = bottomBar.getTabWithId(R.id.tab_lahan);
                nearby.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onTabSelected(@IdRes int tabId) {
        Fragment fragment = null;
        Class fragmentClass;
        switch (tabId){
            case R.id.tab_home:
                fragmentClass = PlayroomFragment.class;
                getSupportActionBar().setTitle("JAGAin");
                break;
            case R.id.tab_lahan:
                fragmentClass = GrowthFragment.class;
                getSupportActionBar().setTitle("Growth");
                break;
            case R.id.tab_notif:
                fragmentClass = NotificationFragment.class;
                getSupportActionBar().setTitle("Notification");
                notifCount = 0;
                break;
            default:
                fragmentClass = ProfileFragment.class;
                getSupportActionBar().setTitle("Profile");
                break;
        }

        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }


        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_frameLayout, fragment).commit();
    }

    @Override
    public void onTabChanged(int no, int count) {
        BottomBarTab nearby = bottomBar.getTabWithId(R.id.tab_notif);
        notifCount += count;
        nearby.setBadgeCount(notifCount);
    }

    @Override
    public void onAlert(String kondisi, String keterangan) {
        new DBHandler(this).add(new Notif(getNextNotifKey()+1, kondisi, keterangan, String.valueOf(R.drawable.icon_circle_clock)));

    }

    public int getNextNotifKey(){
        try{
            Number number = Realm.getDefaultInstance().where(Notif.class).max("id");
            if (number!=null){
                return number.intValue();
            }else{
                return 0;
            }
        }catch (ArrayIndexOutOfBoundsException e){
            return 0;
        }
    }

}
