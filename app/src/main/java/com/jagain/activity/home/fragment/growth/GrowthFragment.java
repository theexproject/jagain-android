package com.jagain.activity.home.fragment.growth;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.jagain.R;
import com.jagain.adapter.GrowthPagerAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abdan on 8/19/2017.
 */

public class GrowthFragment extends Fragment {
    @BindView(R.id.growth_viewpager)
    ViewPager viewPager;

    @BindView(R.id.growth_sliding_tabs)
    TabLayout tabLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_growth, container, false);
        ButterKnife.bind(this, v);

        viewPager.setAdapter(new GrowthPagerAdapter(getChildFragmentManager(),
                getContext()));
        tabLayout.setupWithViewPager(viewPager);
        return v;
    }
}
