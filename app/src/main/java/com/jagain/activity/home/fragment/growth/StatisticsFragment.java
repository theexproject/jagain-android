package com.jagain.activity.home.fragment.growth;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.jagain.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abdan on 8/21/2017.
 */

public class StatisticsFragment extends Fragment {
    @BindView(R.id.stats_tvHappy)
    TextView mHappy;

    @BindView(R.id.stats_tvJoy)
    TextView mJoy;

    @BindView(R.id.stats_tvStress)
    TextView mStres;

    @BindView(R.id.stats_tvAnger)
    TextView mAnger;

    @BindView(R.id.chart)
    PieChart pieChart;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup
            container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_stats, container, false);
        ButterKnife.bind(this, v);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getContext());
        int nHappy = sharedPref.getInt("happy", 0);
        int nJoy = sharedPref.getInt("joy", 0);
        int nStress = sharedPref.getInt("stress", 0);
        int nAnger = sharedPref.getInt("anger", 0);

        mHappy.setText(String.valueOf(nHappy));
        mJoy.setText(String.valueOf(nJoy));
        mStres.setText(String.valueOf(nStress));
        mAnger.setText(String.valueOf(nAnger));

        List<PieEntry> entries = new ArrayList<PieEntry>();
        entries.add(new PieEntry(nHappy, 0));
        entries.add(new PieEntry(nJoy, 1));
        entries.add(new PieEntry(nStress, 2));
        entries.add(new PieEntry(nAnger, 3));

        pieChart.animateY(1000);
        PieDataSet dataSet = new PieDataSet(entries, null);
        PieData data = new PieData(dataSet);
        pieChart.setData(data);
        pieChart.setDescription(null);
        // hide legend
        Legend legend = pieChart.getLegend();
        legend.setEnabled(false);
        dataSet.setColors(new int[]{R.color.mantis, R.color.colorPrimary, R.color.Cournikova, R.color.neon_pink}, getContext()); // set the color<br />
        return v;
    }
}
