package com.jagain.activity.koneksi;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.jagain.activity.home.MainActivity;
import com.jagain.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by abdan on 8/19/2017.
 */

public class KoneksiActivity extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.welcome_btnLanjutkan)
    Button btnLanjutkan;

    int numb;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);
        ButterKnife.bind(this);
        btnLanjutkan.setOnClickListener(this);
        Bundle bundle = getIntent().getExtras();
        if (bundle !=null){
            numb = bundle.getInt("kebiasaan");
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.welcome_btnLanjutkan:
                Intent i = new Intent(this, MainActivity.class);
                i.putExtra("kebiasaan", numb);
                startActivity(i);
                finish();
        }
    }
}
