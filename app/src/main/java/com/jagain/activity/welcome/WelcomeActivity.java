package com.jagain.activity.welcome;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.jagain.R;
import com.jagain.activity.koneksi.KoneksiActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WelcomeActivity extends AppCompatActivity implements View.OnClickListener{
    private final int ROLE_PETANI = 0;
    private final int ROLE_PEMILIKLAHAN = 2;

    @BindView(R.id.welcome_imgCheckPetani)
    ImageView imgPetani;

    @BindView(R.id.welcome_imgCheckPemilikLahan)
    ImageView imgPemilikLahan;

    @BindView(R.id.welcome_btnLanjutkan)
    Button btnSelanjutnya;

    @BindView(R.id.welcome_cdPetani)
    CardView cdPetani;

    @BindView(R.id.welcome_cdPemilikLahan)
    CardView cdPemilikLahan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilihan);
        ButterKnife.bind(this);

        btnSelanjutnya.setOnClickListener(this);
        cdPemilikLahan.setOnClickListener(this);
        cdPetani.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.welcome_cdPemilikLahan:
                imgPetani.setVisibility(View.GONE);
                imgPemilikLahan.setVisibility(View.VISIBLE);
                break;
            case R.id.welcome_cdPetani:
                imgPetani.setVisibility(View.VISIBLE);
                imgPemilikLahan.setVisibility(View.GONE);
                break;
            default:
                Intent intent = new Intent(this, KoneksiActivity.class);
                intent.putExtra("kebiasaan", (imgPemilikLahan.getVisibility() == View.VISIBLE)?1:0);
                startActivity(intent);
        }
    }
}
