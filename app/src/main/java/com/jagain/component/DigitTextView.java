package com.jagain.component;

import android.animation.Animator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.jagain.R;

import java.util.Locale;

/**
 * Created by abdan on 8/19/2017.
 */
public class DigitTextView extends FrameLayout {

    private static int ANIMATION_DURATION = 250;
    TextView currentTextView, nextTextView;

    public DigitTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public DigitTextView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.digit_text_view, this);
        currentTextView = (TextView) getRootView().findViewById(R.id.currentTextView);
        nextTextView = (TextView) getRootView().findViewById(R.id.nextTextView);

        nextTextView.setTranslationY(getHeight());

        setValue(0);
    }

    public void setColor(int color){
        currentTextView.setTextColor(color);
        nextTextView.setTextColor(color);
    }
    public void setNormalValue(double bpmRate){
        currentTextView.setText(String.format(Locale.getDefault(), "%.1f", bpmRate));
    }
    public void setValue(final double desiredValue) {
        if (currentTextView.getText() == null || currentTextView.getText().length() == 0) {
//            currentTextView.setText(String.valueOf(desiredValue));
            currentTextView.setText(String.format(Locale.getDefault(), "%.2f", desiredValue));
        }

        final double oldValue = Double.valueOf(currentTextView.getText().toString());
        double diff = 0;

        if (oldValue > desiredValue) {
            if (oldValue - desiredValue < 1f){
                diff = oldValue - desiredValue;
            } else {
                diff = 1f;
            }
//            nextTextView.setText(String.format(Locale.getDefault(), "%.1f", oldValue-1f));
            nextTextView.setText(String.valueOf(oldValue-diff));

            currentTextView.animate().translationY(-getHeight()).setDuration(ANIMATION_DURATION).start();
            nextTextView.setTranslationY(nextTextView.getHeight());
            final double finalDiff = diff;
            final double finalDiff1 = diff;
            nextTextView.animate().translationY(0).setDuration(ANIMATION_DURATION).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {}
                @Override
                public void onAnimationEnd(Animator animation) {
                    currentTextView.setText(String.valueOf(oldValue- finalDiff));
//                    currentTextView.setText(String.format(Locale.getDefault(), "%.1f", oldValue - 1.0));
                    currentTextView.setTranslationY(0);
                    if (oldValue - finalDiff1 != desiredValue) {
                        setValue(desiredValue);
                    }
                }
                @Override
                public void onAnimationCancel(Animator animation) {}
                @Override
                public void onAnimationRepeat(Animator animation) {}
            }).start();
        } else if (oldValue < desiredValue) {
            if (desiredValue - oldValue < 1f) {
                diff = desiredValue - oldValue;
            }else {
                diff = 1f;
            }

            nextTextView.setText(String.valueOf(oldValue+diff));
//            nextTextView.setText(String.format(Locale.getDefault(), "%.1f", oldValue+1.0));

            currentTextView.animate().translationY(getHeight()).setDuration(ANIMATION_DURATION).start();
            nextTextView.setTranslationY(-nextTextView.getHeight());
            final double finalDiff2 = diff;
            nextTextView.animate().translationY(0).setDuration(ANIMATION_DURATION).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {}
                @Override
                public void onAnimationEnd(Animator animation) {
                    currentTextView.setText(String.valueOf(oldValue + finalDiff2));
//                    currentTextView.setText(String.format(Locale.getDefault(), "%.1f", oldValue + 1f));
                    currentTextView.setTranslationY(0);
                    if (oldValue + finalDiff2 != desiredValue) {
                        setValue(desiredValue);
                    }
                }
                @Override
                public void onAnimationCancel(Animator animation) {}
                @Override
                public void onAnimationRepeat(Animator animation) {}
            }).start();
        }
    }
}